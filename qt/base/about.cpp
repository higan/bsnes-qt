#include "about.moc"
AboutWindow *aboutWindow;

AboutWindow::AboutWindow() {
  setObjectName("about-window");
  setWindowTitle("About");
  setGeometryString(&config().geometry.aboutWindow);
  application.windowList.append(this);

  #if 0
  #if defined(DEBUGGER)
  setStyleSheet("background: #c0c080");
  #elif defined(PROFILE_ACCURACY)
  setStyleSheet("background: #c08080");
  #elif defined(PROFILE_COMPATIBILITY)
  setStyleSheet("background: #8080c0");
  #elif defined(PROFILE_PERFORMANCE)
  setStyleSheet("background: #80c080");
  #endif
  #endif

  layout = new QVBoxLayout;
  layout->setSizeConstraint(QLayout::SetFixedSize);
  layout->setMargin(Style::WindowMargin);
  layout->setSpacing(Style::WidgetSpacing);
  setLayout(layout);

  logo = new Logo;
  logo->setFixedSize(600, 106);
  layout->addWidget(logo);

  struct retro_system_info retro_info;
  retro_get_system_info(&retro_info);
  const char *library_id = retro_info.library_name ? retro_info.library_name : "Unknown";
  const char *library_ver = retro_info.library_version ? retro_info.library_version : "Unknown version";

  info = new QLabel(string() <<
    "<table width='100%'><tr>"
    "<td align='left'><b>Version: </b>" << ProgramVersion << "</td>"
    "<td align='left'><b>Core: </b>" << library_id << " " << library_ver << "</td>"
    "<td align='center'><b>Author: </b>byuu</td>"
    "<td align='right'><b>Homepage: </b><a href='http://byuu.org'>http://byuu.org</a></td>"
    "</tr></table>"
  );
  layout->addWidget(info);
}

void AboutWindow::Logo::paintEvent(QPaintEvent*) {
  QPainter painter(this);
  QPixmap pixmap(":/logo.png");
  painter.drawPixmap(0, 0, pixmap);
}
