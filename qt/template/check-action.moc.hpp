#ifndef NALL_QT_CHECKACTION_HPP
#define NALL_QT_CHECKACTION_HPP

namespace nall {

class CheckAction : public QAction {
  Q_OBJECT

public:
  CheckAction(const QString&, QObject*);

protected slots:

protected:
};

inline CheckAction::CheckAction(const QString &text, QObject *parent) : QAction(text, parent) {
  setCheckable(true);
}

}

#endif
