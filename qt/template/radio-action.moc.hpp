#ifndef NALL_QT_RADIOACTION_HPP
#define NALL_QT_RADIOACTION_HPP

namespace nall {

class RadioAction : public QAction {
  Q_OBJECT

public:
  RadioAction(const QString&, QObject*);

protected slots:

protected:
  QActionGroup *actiongroup;
};

inline RadioAction::RadioAction(const QString &text, QObject *parent) : QAction(text, parent) {
  setCheckable(true);
  actiongroup = new QActionGroup(this);
  actiongroup->addAction(this);
}

}

#endif
