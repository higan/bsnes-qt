class ToolsWindow : public Window {
  Q_OBJECT

public:
  QVBoxLayout *layout;
  QTabWidget *tab;
  QScrollArea *cheatEditorArea;
  QScrollArea *cheatFinderArea;
  QScrollArea *stateManagerArea;
  #if 0
  QScrollArea *effectToggleArea;
  #endif

  ToolsWindow();

public slots:
};

extern ToolsWindow *toolsWindow;
