namespace bsnesqt {

  void bsnes_video_refresh(const void *data, unsigned width, unsigned height, size_t pitch);
  void bsnes_audio_sample(int16_t left, int16_t right);
  void bsnes_input_poll();
  int16_t bsnes_input_state(bool port, unsigned device, unsigned index, unsigned id);

  class Interface : public library {
    public:
      void video_refresh(const void *data, unsigned width, unsigned height, size_t pitch);
      void audio_sample(int16_t left, int16_t right);
      void input_poll();
      int16_t input_poll(unsigned port, unsigned device, unsigned index, unsigned id);

      Interface();
      void captureScreenshot(uint32_t*, unsigned, unsigned, unsigned);
      bool saveScreenshot;
      bool framesUpdated;
      unsigned framesExecuted;
  };

  extern Interface interface;

}
